//
//  General.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 17/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation

struct General: Codable{
    
    var pokemon: [Pokemon]?
    
    enum CodingKeys: String, CodingKey {

        case pokemon = "pokemon"

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pokemon = try values.decodeIfPresent([Pokemon].self, forKey: .pokemon)
    }
}

// MARK: Ability convenience initializers and mutators
