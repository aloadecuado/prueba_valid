//
//  CustomBackgrundView.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 12/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit

class CustomBackgrundView:UIView{
    
    var gl:CAGradientLayer!
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
        


        self.gl = CAGradientLayer()
        self.gl.colors = [UIColor(rgb: 0x6E95FD).cgColor, UIColor(rgb: 0x6FDEFA).cgColor, UIColor(rgb: 0x8DE061).cgColor, UIColor(rgb: 0x51E85E).cgColor]
        self.gl.locations = [0.0, 1.0]
        
        self.layer.insertSublayer(self.gl, at: 0)
    }
    
}
