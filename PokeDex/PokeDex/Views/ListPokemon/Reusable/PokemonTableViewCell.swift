//
//  PokemonTableViewCell.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 18/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyGif

protocol PokemonTableViewCellDelegate {
    func succesGetAllPokemon(pokemons:Pokemon)
    func showError(error:String)
    
}
class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var spinnerView:UIView!

    
    @IBOutlet weak var pokemonImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var codeLabel:UILabel!
    @IBOutlet weak var face1ImageView:UIImageView!
    @IBOutlet weak var face2ImageView:UIImageView!
    
    private var controller:ListPokemonViewController?
    var listPokemonPresenter: ListPokemonPresenter?
    override func awakeFromNib() {
        super.awakeFromNib()

        let screenWidth = spinnerView.frame.width
        let screenHeight = spinnerView.frame.height
        
        let wGif:CGFloat = 50
        let hGif:CGFloat = wGif
        let xGif:CGFloat = (screenWidth / 2) - (wGif / 2)
        let yGif:CGFloat = (screenHeight / 2) - (hGif / 2)
        
        do {
            let gif = try UIImage(gifName: "pikachu.gif")
            let imageview = UIImageView(gifImage: gif, loopCount: -1) // Use -1 for infinite loop
            imageview.frame = CGRect(x: xGif, y: yGif, width: wGif, height: hGif)
            spinnerView.addSubview(imageview)
            
        } catch {
            print(error)
        }
        listPokemonPresenter = ListPokemonPresenter(pokemonTableViewCell: self)
        
        // Initialization code
    }

    func setData(controller: ListPokemonViewController, id: Int){
        spinnerView.isHidden = false
        self.controller = controller
        listPokemonPresenter?.getAllPokemon(controller: self.controller!, id: id)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension PokemonTableViewCell: PokemonTableViewCellDelegate{
    func succesGetAllPokemon(pokemons: Pokemon) {
        spinnerView.isHidden = true
        if let url1 = URL(string: (pokemons.sprites?.front_default)!) {
            pokemonImageView.sd_setImage(with: url1)
        }
        
        nameLabel.text = pokemons.name!
        var idText = ""
        
        if "\(String(describing: pokemons.id!))".count == 1 {
            idText = "#00\(String(describing: pokemons.id!))"
        }else if "\(String(describing: pokemons.id!))".count == 2{
            idText = "#0\(String(describing: pokemons.id!))"
        }else if "\(String(describing: pokemons.id!))".count == 3{
            idText = "#\(String(describing: pokemons.id!))"
        }
        codeLabel.text = idText
    }
    
    func showError(error: String) {
        
    }
    
    
}
