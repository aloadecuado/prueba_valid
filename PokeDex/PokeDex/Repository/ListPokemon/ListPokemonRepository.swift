//
//  ListPokemonRepository.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 17/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation

//MARK: -Init Repository
class ListPokemonRepository{

}

//MARK: -Storage
extension ListPokemonRepository{
    
    func getAllPokemon(id:Int, Ok: @escaping ((Pokemon) -> Void), Error: @escaping ((String) -> Void)){
        let url = getIdInfoPlist(key: "KURL_GET_POKEMON")
        
        getReturnData(url: url + "\(id)", Ok: { (data) in
            
            do{
                let general = try JSONDecoder().decode(Pokemon.self, from: data)
                Ok(general)
            }catch{
                Error(error.localizedDescription)
            }
            
            
        }) { (error, data) in

            Error("Ha ocurrido un error")
        }
    }
}
