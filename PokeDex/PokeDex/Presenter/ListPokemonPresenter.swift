//
//  ListPokemonPresenter.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 18/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit
protocol ListPokemonPresenterDelegate {
    func succesGetAllPokemon(pokemons:Pokemon)
    func showError(error:String)
    
    func getAllPokemon(controller:UIViewController, id:Int)
}
//MARK: -Init Presenter
class ListPokemonPresenter {
    var pokemonTableViewCell: PokemonTableViewCell?
    var listPokemonInteractor: ListPokemonInteractor?
    init(pokemonTableViewCell: PokemonTableViewCell) {
        self.pokemonTableViewCell = pokemonTableViewCell
        self.listPokemonInteractor = ListPokemonInteractor(listPokemonPresenter: self)
    }
}
//MARK: -Presenter

extension ListPokemonPresenter:ListPokemonPresenterDelegate{
    func succesGetAllPokemon(pokemons: Pokemon) {
        self.pokemonTableViewCell?.succesGetAllPokemon(pokemons: pokemons)
    }
    
    func showError(error: String) {
        self.pokemonTableViewCell?.showError(error: error)
    }
    

    
    func getAllPokemon(controller: UIViewController, id:Int) {
        self.listPokemonInteractor?.getAllPokemon(controller: controller, id: id)
    }
    
}

