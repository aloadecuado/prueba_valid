//
//  PokemonInteractor.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 18/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit
//MARK: -Delegate
protocol ListPokemonInteractorDelegate {
    func getAllPokemon(controller:UIViewController, id:Int)
}
//MARK: -Init Interactor
class ListPokemonInteractor{
    var listPokemonPresenter:ListPokemonPresenter?
    var listPokemonRepository:ListPokemonRepository?
    init(listPokemonPresenter:ListPokemonPresenter) {
        self.listPokemonPresenter = listPokemonPresenter
        self.listPokemonRepository = ListPokemonRepository()
    }

}

//MARK: -Repository
extension ListPokemonInteractor: ListPokemonInteractorDelegate{
    func getAllPokemon(controller:UIViewController, id:Int){
        //SwiftSpinner.show()
        listPokemonRepository?.getAllPokemon(id: id, Ok: {pokemons in
            //SwiftSpinner.hide()
            self.listPokemonPresenter?.succesGetAllPokemon(pokemons: pokemons)
        }, Error: {error in
            //SwiftSpinner.hide()
            self.listPokemonPresenter?.showError(error: error)
        })
    }
}
