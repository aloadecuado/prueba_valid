//
//  CustomGetResponse.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 17/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit


func getReturnData(url:String, Ok:@escaping((Data) -> Void), Error:@escaping ((Error?, Data?) -> Void) )
{
    

        getApi(url: url,headers:nil, statusCodeCorrect: [200], Ok: {data in

            Ok(data)
        }, Error: {(err, data) in

            Error(err,data)
        })

}
