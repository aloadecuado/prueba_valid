//
//  CustomPostResponse.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 17/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit

func postReturnData(url:String, parameters:NSDictionary, Ok:@escaping((Data) -> Void), Error:@escaping ((Error?, Data?) -> Void) )
{

        postApi(url: url, statusCodeCorrect: [200], headers: nil, parameters: parameters, Ok: {data in

            Ok(data)
        }, Error: {error,data  in

            Error(error,data)
            
        })


}

