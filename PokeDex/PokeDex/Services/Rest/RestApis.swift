//
//  RestApis.swift
//  PokeDex
//
//  Created by Pedro Alonso Daza B on 17/05/20.
//  Copyright © 2020 Pedro Alonso Daza B. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

func getApi(url:String, headers:Dictionary<String, String>?, statusCodeCorrect:[Int], Ok:@escaping((Data) -> Void), Error:@escaping ((Error?, Data?) -> Void)){
    
    
    request(url, method: .get, encoding: URLEncoding.default, headers: headers)
        .responseData {response in
            
            if let err = response.result.error {
                if let errorCode = response.response{
                    print("error code: \(errorCode.statusCode)")
                    print("error: \(err.localizedDescription)")
                    NSLog("getApi", "description: \(err.localizedDescription) code: \(errorCode.statusCode)")
                }
                Error(err, nil)
            }else{

                if let data = response.data{

                    if let responseOk = response.response{
                        var isCorrect = false

                        for status in statusCodeCorrect{
                            if status == responseOk.statusCode{
                                isCorrect = true
                            }
                        }

                        if isCorrect{
                            Ok(data)
                        }else{
                            NSLog("getApi", "Error contact: " + response.debugDescription)

                            Error(response.error, data)
                        }
                    }else{
                        NSLog("getApi", "Error contact: " + response.debugDescription)

                        Error(response.error, data)
                    }
                }
            }
    }
}

func postApi(url:String, statusCodeCorrect:[Int], headers:Dictionary<String, String>?,parameters:NSDictionary?, Ok:@escaping((Data) -> Void), Error:@escaping ((Error?, Data?) -> Void) )
{

    request(url, method: .post, parameters: parameters as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
        
        if let err = response.result.error {
            if let errorCode = response.response{
                print("error code: \(errorCode.statusCode)")
                print("error: \(err.localizedDescription)")
                NSLog("getApi", "description: \(err.localizedDescription) code: \(errorCode.statusCode)")
            }
            Error(err, nil)
        }
        else {
            if let data = response.data{

                if let responseOk = response.response{
                    var isCorrect = false

                    for status in statusCodeCorrect{
                        if status == responseOk.statusCode{
                            isCorrect = true
                        }
                    }

                    if isCorrect{
                        Ok(data)
                    }else{
                        NSLog("getApi", "Error contact: " + response.debugDescription)
                        if let err = response.error{
                            Error(err , data)
                        }else{
                            Error(nil , data)
                        }
                    }
                }else{
                    NSLog("getApi", "Error contact: " + response.debugDescription)

                    if let err = response.error{
                        Error(err , data)
                    }else{
                        Error(nil , data)
                    }
                }

            }
        }
    }
}

func deleteApi(url:String, statusCodeCorrect:[Int], headers:Dictionary<String, String>?,parameters:NSDictionary?, Ok:@escaping((Data) -> Void), Error:@escaping ((Error?, Data?) -> Void) )
{
    
    request(url, method: .delete, parameters: parameters as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
        
        if let err = response.result.error {
            if let errorCode = response.response{
                print("error code: \(errorCode.statusCode)")
                print("error: \(err.localizedDescription)")
                NSLog("getApi", "description: \(err.localizedDescription) code: \(errorCode.statusCode)")
            }
            Error(err, nil)
        }
        else {
            if let data = response.data{

                if let responseOk = response.response{
                    var isCorrect = false

                    for status in statusCodeCorrect{
                        if status == responseOk.statusCode{
                            isCorrect = true
                        }
                    }

                    if isCorrect{
                        Ok(data)
                    }else{
                        NSLog("getApi", "Error contact: " + response.debugDescription)
                        if let err = response.error{
                            Error(err , data)
                        }else{
                            Error(nil , data)
                        }

                    }
                }else{
                    NSLog("getApi", "Error contact: " + response.debugDescription)

                    if let err = response.error{
                        Error(err , data)
                    }else{
                        Error(nil , data)
                    }
                }
            }
        }
    }
}
